
Diericx.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

Diericx.Preloader.prototype = {

	preload: function () {

		//	These are the assets we loaded in Boot.js
		//	A nice sparkly background and a loading progress bar
		this.background = this.add.sprite(0, 0, 'preloaderBackground');
		//this.preloadBar = this.add.sprite(300, 400, 'preloaderBar');

		//	This sets the preloadBar sprite as a loader sprite.
		//	What that does is automatically crop the sprite from 0 to full-width
		//	as the files below are loaded in.
		//this.load.setPreloadSprite(this.preloadBar);

		this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
		//	Here we load the rest of the assets our game needs.
		//	As this is just a Project Template I've not provided these assets, swap them for your own.
		this.load.image('background','images/darkPurple.png');

		this.load.image('titlepage', 'images/title.jpg');
		this.load.spritesheet('button', 'images/button-long-down.png', 190, 49);
		this.load.image('player', 'images/ship-small.png');

		//Enemies
		this.load.spritesheet('eyeBoss', 'images/eyeBoss.png', 280, 255, 4);
		this.load.spritesheet('eyeEnemy-small', 'images/eyeEnemy-small.png', 69.5, 50, 2);
		this.load.spritesheet('dilly', 'images/dilly.png', 193, 138, 3);

		//bullets
		this.load.spritesheet('bullet', 'images/bullet.png', 9, 18, 1);
		this.load.spritesheet('bullet-rainbow', 'images/bullet-rainbow.png', 9, 18, 6);
		this.load.spritesheet('laser-rainbow', 'images/laser-rainbow.png', 2, 500, 5);

		//UI
		this.load.spritesheet('button-long', 'images/ui/button-long.png', 190, 49);
		this.load.spritesheet('button-square', 'images/ui/button-square-icon.png', 45, 49);
		this.load.image('health-bar-bg', 'images/ui/health-bar-bg.png');
		this.load.image('health-bar', 'images/ui/health-bar.png');
		this.load.image('inventory-bar', 'images/ui/inventory-bar.png');
		this.load.image('inventory-empty-bg', 'images/ui/inventory-empty-bg.png');

		//this.load.atlas('playButton', 'images/button-long-down.png');
		// this.load.audio('titleMusic', ['audio/main_menu.mp3']);
		// this.load.bitmapFont('caslon', 'fonts/caslon.png', 'fonts/caslon.xml');

		//	+ lots of other required assets here

	},

	create: function () {

		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
		this.preloadBar.cropEnabled = false;

	},

	update: function () {

		//	You don't actually need to do this, but I find it gives a much smoother game experience.
		//	Basically it will wait for our audio file to be decoded before proceeding to the MainMenu.
		//	You can jump right into the menu if you want and still play the music, but you'll have a few
		//	seconds of delay while the mp3 decodes - so if you need your music to be in-sync with your menu
		//	it's best to wait for it to decode here first, then carry on.

		//	If you don't have any music in your game then put the game.state.start line into the create function and delete
		//	the update function completely.

		// if (this.cache.isSoundDecoded('titleMusic') && this.ready == false)
		// {
			this.ready = true;
			this.state.start('MainMenu');
		// }

	}

};
