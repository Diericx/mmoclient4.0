var upKey;
var downKey;
var leftKey;
var rightKey;
var space;

var line1;

function distance(e1, e2) {
  return Math.sqrt( Math.pow(e2.x - e1.x, 2) + Math.pow(e2.y - e1.y, 2) )
}

Diericx.Game = function (game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;      //  a reference to the currently running game (Phaser.Game)
    this.add;       //  used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera;    //  a reference to the game camera (Phaser.Camera)
    this.cache;     //  the game cache (Phaser.Cache)
    this.input;     //  the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load;      //  for preloading assets (Phaser.Loader)
    this.math;      //  lots of useful common math operations (Phaser.Math)
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage;     //  the game stage (Phaser.Stage)
    this.time;      //  the clock (Phaser.Time)
    this.tweens;    //  the tween manager (Phaser.TweenManager)
    this.state;     //  the state manager (Phaser.StateManager)
    this.world;     //  the game world (Phaser.World)
    this.particles; //  the particle manager (Phaser.Particles)
    this.physics;   //  the physics manager (Phaser.Physics)
    this.rnd;       //  the repeatable random number generator (Phaser.RandomDataGenerator)

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

};

Diericx.Game.prototype = {

    create: function () {
      var game = this
      //bg
      bg = this.add.tileSprite(-1500, -1500, 3000, 3000, 'background');
      //groups
      this.spriteGroup = this.add.group()
      this.uiGroup = this.add.group()
      //settings
      this.camera.bounds = null;
      $.getJSON("scripts/ObjectData.json", function(json) {
        game.objData = json; // this will show the info it in firebug console
      });

      Leaderboard.init(this, this.world.width - 120, 10)
      HUD.init(this)

      Server.init(this, "ws://localhost:7777/")

      UI.init(this, Server)

      //graphics layer for the lines
      this.graphics = this.add.graphics(0, 0);

      //create player
      this.currentPlayer = this.add.sprite(0, 0, 'player');
      this.currentPlayer.anchor.x = 0.5;
      this.currentPlayer.anchor.y = 0.5;
      this.spriteGroup.add(this.currentPlayer)
      Diericx.currentPlayer = this.currentPlayer

      var ref = new Firebase("https://diericx.firebaseio.com/messages");
      var messageField = $('#messageInput');
      var messageList = $('#messages');

      // LISTEN FOR KEYPRESS EVENT
      messageField.keypress(function (e) {
        if (e.keyCode == 13) {
          //FIELD VALUES
          var username = game.currentPlayer.username;
          var message = messageField.val();

          console.log(game.currentPlayer.username)

          //SAVE DATA TO FIREBASE AND EMPTY FIELD
          ref.push({name:username, text:message});
          messageField.val('');
        }
      });

      // Add a callback that is triggered for each chat message.
      ref.limitToLast(10).on('child_added', function (snapshot) {
        //GET DATA
        var data = snapshot.val();
        var username = data.name || "anonymous";
        var message = data.text;

        //CREATE ELEMENTS MESSAGE & SANITIZE TEXT
        var messageElement = $("<li>");
        var nameElement = $("<strong class='chat-username'></strong>")
        nameElement.text(username);
        messageElement.text(message).prepend(nameElement);

        //ADD MESSAGE
        messageList.append(messageElement)

        //SCROLL TO BOTTOM OF MESSAGE LIST
        messageList[0].scrollTop = messageList[0].scrollHeight;
      });

      $('#chat-form').submit(function () {
        return false;
      });

      //healthBar.crop.width = (50 / 100) * healthBar.width

      //Current player UI
      // this.idText = this.add.text(0, 0, "id: ");
      // this.idText.anchor.setTo(0, 0);
      // this.idText.font = 'Revalia';
      // this.idText.fontSize = 20;
      // this.idText.fixedToCamera = true;
      //
      // this.parentTxt = this.add.text(0, 30, "parent: ");
      // this.parentTxt.anchor.setTo(0, 0);
      // this.parentTxt.font = 'Revalia';
      // this.parentTxt.fontSize = 20;
      // this.parentTxt.fixedToCamera = true;
      //
      // this.childTxt = this.add.text(0, 60, "child: ");
      // this.childTxt.anchor.setTo(0, 0);
      // this.childTxt.font = 'Revalia';
      // this.childTxt.fontSize = 20;
      // this.childTxt.fixedToCamera = true;
      //
      // this.heightTxt = this.add.text(0, 90, "dmg multiplier: ");
      // this.heightTxt.anchor.setTo(0, 0);
      // this.heightTxt.font = 'Revalia';
      // this.heightTxt.fontSize = 20;
      // this.heightTxt.fixedToCamera = true;
      //Server.uiGroup.add(text)

      //set keys
      // upKey = this.input.keyboard.addKey(Phaser.Keyboard.W);
      // downKey = this.input.keyboard.addKey(Phaser.Keyboard.S);
      // leftKey = this.input.keyboard.addKey(Phaser.Keyboard.A);
      // rightKey = this.input.keyboard.addKey(Phaser.Keyboard.D);
      // space = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    },

    update: function () {

      //move camera
      this.camera.x = this.currentPlayer.x - this.world.centerX;
      this.camera.y = this.currentPlayer.y - this.world.centerY;

      //rotate player
      x = this.world.centerX;
      y = this.world.centerY;
      mx = this.input.x;
      my = this.input.y;
      dx = mx - x;
      dy = my - y;
      this.mouseAngle = Math.atan2(dy, dx)
      this.currentPlayer.angle = this.mouseAngle  * (180/Math.PI)
      //game.scale.startFullScreen(false);

      //set movement keys
      if (!$("#messageInput").is(":focus")) {
        if (this.input.keyboard.isDown(Phaser.Keyboard.W))
          this.currentPlayer.movY = -1;
        else if (this.input.keyboard.isDown(Phaser.Keyboard.S))
          this.currentPlayer.movY = 1;
        else
          this.currentPlayer.movY = 0;

        if (this.input.keyboard.isDown(Phaser.Keyboard.A))
          this.currentPlayer.movX = -1
        else if (this.input.keyboard.isDown(Phaser.Keyboard.D))
          this.currentPlayer.movX = 1
        else
          this.currentPlayer.movX = 0

        //shoot alt
        if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
          var message = {};
          message.Action = "shoot-alt"
          this.sock.send(JSON.stringify(message))
        }
      }

      //shoot main
      if (this.input.activePointer.leftButton.isDown) {
        var message = {};
        message.Action = "shoot"
        this.sock.send(JSON.stringify(message))
      }

      //check objects time stamps
      Server.checkForExpiredObjects()

      HUD.update()

      UI.update()

      //send data
      this.sendData()

    },

    toggleFullscreen: function() {
      if (this.scale.isFullScreen)
      {
         this.scale.stopFullScreen();
      }
      else
      {
         this.scale.startFullScreen(false);
      }
    },

    render: function() {
      //this.debug.geom(line1);
    },

    sendData: function () {
      if (this.currentPlayer != null) {
        var message = {};
        message.Action = "update"
        message.MouseAngle = this.mouseAngle
        message.Token = Diericx.Token
        message.Uid = Diericx.Uid
        message.MovX = this.currentPlayer.movX
        message.MovY = this.currentPlayer.movY

        if (this.sock != null && this.sock.readyState == 1) {
          this.sock.send(JSON.stringify(message))
        }
      }
      //console.log("GOT EHRE GO GADFS")
      //setTimeout(this.sendData, 30);
    },

};
