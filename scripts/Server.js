var Server = {}
Server.players = new Array();
Server.gameObjects = new Array();


var findObject = function(id, array) {
  for (var i = 0; i < array.length; i++) {
    if (array[i].id == id) {
      return array[i];
    };
  }
  //if no player was found
  return null
}

Server.init = function (game, ip ) {
  //socket
  var sock = new WebSocket(ip); //73.158.248.23
  sock.onmessage = function(m) {Server.receiveData(m, game)}
  game.sock = sock
}

Server.createNewPlayer = function(g, id, username) {
  var player = {};
  player.id = id;
  player.health = 0;
  player.username = username
  player.entityType = "Player"
  player.entity = g.add.sprite(0, 0, 'player');
  player.entity.anchor.x = 0.5;
  player.entity.anchor.y = 0.5;
  g.spriteGroup.add(player.entity)
  player.lastUpdate = new Date().getTime()
  Server.players.push(player)
  //g.updatePlayerList()
  return player;
}

Server.createNewGameObject = function (g, id, tag) {
  var obj = new Object();
  obj.id = id;
  obj.entityType = "Object"

  var data = g.objData[tag]
  if (data != null) {
    obj.entity = g.add.sprite(0, 0, data.sprite.file);
    obj.entity.anchor.x = 0.5
    obj.entity.anchor.y = 0.5
    obj.entity.animations.add('idle', data.sprite.frames, data.sprite.animTime, true);
    obj.entity.animations.play('idle');
    g.spriteGroup.add(obj.entity)
  }

  obj.lastUpdate = new Date().getTime()
  Server.gameObjects.push(obj)
  return obj;
}

Server.checkForExpiredObjects = function() {
  for (i = 0; i < this.gameObjects.length; i++) {
    obj = this.gameObjects[i];
    now = new Date().getTime();

    if ( (now - obj.lastUpdate) > 100) {
      this.gameObjects[i].entity.destroy()
      this.gameObjects.splice(i, 1);
      return this.checkForExpiredObjects();
    }
  }

  for (i = 0; i < this.players.length; i++) {
    obj = this.players[i];
    now = new Date().getTime();

    if ( (now - obj.lastUpdate) > 100) {
      this.players[i].entity.destroy()
      this.players.splice(i, 1);
      UI.updateNearbyPlayerList()
      return this.checkForExpiredObjects();
    }
  }

  return true;
}

Server.receiveData = function(m, game) {
  var packet = JSON.parse(m.data)
  // //Edit main player
  // game.idText.text = "id: " + packet.Cp.Id
  // game.parentTxt.text = "parent: " + packet.Cp.Parent
  // game.childTxt.text = "child: " + packet.Cp.Child
  // game.heightTxt.text = "dmg multiplier: " + packet.Cp.Height

  Leaderboard.update(packet.Leaderboard)
  game.currentPlayer.id = packet.Cp.Id
  game.currentPlayer.parentId = packet.Cp.Parent
  game.currentPlayer.childId = packet.Cp.Child
  game.currentPlayer.username = packet.Cp.Username
  game.currentPlayer.score = packet.Cp.Score
  game.currentPlayer.health = packet.Cp.Health
  game.currentPlayer.power = packet.Cp.Power
  game.currentPlayer.maxPower = packet.Cp.MaxPower
  game.currentPlayer.x = packet.Cp.X
  game.currentPlayer.y = packet.Cp.Y
  game.currentPlayer.inventory = packet.Cp.Items
  //Diericx.currentPlayer.angle = packet.Cp.Rot * (180/Math.PI)
  game.currentPlayer.targets = packet.Cp.Targets
  //console.log("X: " + packet.Cp.X + ", Y: " + packet.Cp.Y)
  //Edit other players
  if (packet.Objects != null) {
    for (i = 0; i < packet.Objects.length; i++) {
      var packObj = packet.Objects[i]
      var gameObj = {}
      //players
      if (packObj["Type"] == "Player") {

        gameObj = findObject(packObj.Id, this.players)
        //create new player if none wer found
        if (gameObj == null) {
          console.log("new player")
          gameObj = Server.createNewPlayer(game, packObj.Id, packObj.Username)
        }
      }
      //bullets, npcs etc.
      else if (packObj.Type == "GameObject" || packObj.Type == "Bullet" || packObj.Type == "Npc") {
        gameObj = findObject(packObj.Id, this.gameObjects)
        if (gameObj == null) {
          gameObj = Server.createNewGameObject(game, packObj.Id, packObj.Tag)
        }
      }

      if (gameObj) {
        gameObj.entity.entityType = packObj.Type
        gameObj.username = packObj.Username
        gameObj.entity.parentId = packObj.Parent
        gameObj.entity.childId = packObj.Child
        gameObj.entity.health = packObj.Health
        gameObj.entity.healthCap = packObj.HealthCap
        gameObj.entity.power = packObj.Power
        gameObj.entity.x = packObj.X
        gameObj.entity.y = packObj.Y
        gameObj.entity.angle = packObj.Rot * (180/Math.PI)
        gameObj.lastUpdate = new Date().getTime()
      }
    }
  }
}
