var HUD = {}

HUD.init = function(game) {
  HUD.game = game
  healthBarBg = game.add.sprite(0, 0, "health-bar-bg");
  healthBarBg.anchor.y = 0
  healthBarBg.scale.setTo(0.2, 0.2);
  healthBarBg.fixedToCamera = true;
  game.uiGroup.add(healthBarBg)

  healthBar = game.add.sprite(healthBarBg.x + 47, healthBarBg.y + 13, "health-bar");
  healthBar.anchor.x = 0
  healthBar.anchor.y = 0
  healthBar.scale.setTo(0.2, 0.2);
  healthBar.fixedToCamera = true;
  game.uiGroup.add(healthBar)
  healthBar.cropEnabled = true

  healthBarCrop = new Phaser.Rectangle(0, 0, HUD.calcWidth(healthBar.width, healthBar, 1), healthBar.height*5);
  healthBarCrop.origW = healthBar.width
  healthBar.crop(healthBarCrop)
  this.healthBar = healthBar
  this.healthBarCrop = healthBarCrop
  this.healthBar.updateCrop();

  bottomBar = game.add.sprite(game.world.width/2, game.world.height - 50, "inventory-bar");
  bottomBar.scale.setTo(0.25, 0.25);
  bottomBar.anchor.y = 0
  bottomBar.anchor.x = 0.5

  bottomBar.fixedToCamera = true;
  game.uiGroup.add(bottomBar)
}

HUD.calcWidth = function(origW, obj, scale) {
  return origW*(1/obj.scale.x)*scale
}

HUD.update = function() {

  var newW = HUD.calcWidth(healthBarCrop.origW, healthBar, HUD.game.currentPlayer.health/100)
  if (newW != this.healthBarCrop.width) {
    this.healthBarCrop.width = newW
    this.healthBar.updateCrop();
  }
}
