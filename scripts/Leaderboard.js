var Leaderboard = {}

Leaderboard.init = function(game, x, y) {
  Leaderboard.game = game
  Leaderboard.names = {}
  Leaderboard.scores = {}

  var title = game.add.text(x, y, "-Leaderboard-", {fill: '#fff'});
  title.anchor.setTo(0.5, 0);
  title.font = 'Revalia';
  title.fontSize = 20;
  title.fixedToCamera = true;
  game.uiGroup.add(title)

  for (i = 0; i < 5; i++) {
    var newN = game.add.text(x - 100, y + ((i+1)*30), "#" + i, {fill: '#27ae60'});
    newN.anchor.setTo(0, 0);
    newN.font = 'Revalia';
    newN.fontSize = 15;
    newN.fixedToCamera = true;
    Leaderboard.names[i] = newN
    game.uiGroup.add(newN)

    var newS = game.add.text(x + 100, y + ((i+1)*30), "", {fill: '#27ae60'});
    newS.anchor.setTo(1, 0);
    newS.font = 'Revalia';
    newS.fontSize = 15;
    newS.fixedToCamera = true;
    Leaderboard.scores[i] = newS
    game.uiGroup.add(newS)
  }
}

Leaderboard.update = function(lb) {
  for (i = 0; i < lb.length; i ++) {
    Leaderboard.names[i].text = "#" + (i+1) + "   " + lb[i].Username
    Leaderboard.scores[i].text = lb[i].Score
  }
}
