var UI = {}
UI.playerList = new Array();
UI.invButtons = new Array();

UI.init = function(g) {
  UI.g = g
  UI.s = Server

  //create player list items
  var yOffset = 300
  nearbyPlayersTxt = g.add.text(g.world.width - 50, yOffset - 50, "-Nearby Players-", {fill: '#D4D4D4'});
  nearbyPlayersTxt.anchor.setTo(1, 0);
  nearbyPlayersTxt.font = 'Revalia';
  nearbyPlayersTxt.fontSize = 20;
  nearbyPlayersTxt.fixedToCamera = true;

  this.createInvButtons( 170, g.world.height - 80);

  for (i = 0; i < 5; i ++) {
    onConnectClick = function(e) {
      var message = {};
      message.Action = "setParent"
      message.Value = e.val
      console.log("Setting parent: ", e.val)
      g.sock.send(JSON.stringify(message))
    }

    //add button
    button = g.add.button(g.world.width - 60, 60*i + yOffset, 'button-square', onConnectClick, this, 0, 0, 1);
    button.inputEnabled = true;
    button.fixedToCamera = true;
    g.uiGroup.add(button)
    //add text
    text = g.add.text(g.world.width - 100, 60*i + yOffset + 40, "p.username", {fill: '#3498db'});
    text.anchor.setTo(0.5, 1);
    text.font = 'Revalia';
    text.fontSize = 20;
    text.fixedToCamera = true;
    g.uiGroup.add(text)

    playerListItem = {}
    playerListItem.btn = button
    playerListItem.txt = text
    this.playerList.push(playerListItem)
  }
}

UI.update = function() {
  //update player list
  UI.updateNearbyPlayerList()

  //draw connection lines
  UI.drawConnectionLines()

  if (this.g.currentPlayer.inventory) {
    // for (i = 0; i < this.g.currentPlayer.inventory.size(); i++) {
    //   var item = this.g.currentPlayer.inventory[i];
    //   consoel.log(item)
    // }
    for (i in this.g.currentPlayer.inventory) {
      var item = this.g.currentPlayer.inventory[i];
      var invBtn = this.invButtons[i]
      if (!invBtn.icon) {
        var file = ""
        if (this.g.objData[String(item)]) {
          file = this.g.objData[String(item)].sprite.file
        }
        var icon = this.g.add.sprite(65*i + invBtn.xOffset + 20, invBtn.yOffset + 15, file);
        icon.anchor.x = 0
        icon.anchor.y = 0
        icon.fixedToCamera = true;
        this.g.uiGroup.add(icon)
        invBtn.icon = icon
        invBtn.icon.tag = item
      } else {
        //console.log("invBtn.icon.tag:" + invBtn.icon.tag, "Item: " + item)
        if (invBtn.icon.tag != item) {
          invBtn.icon.destroy()
          var icon = this.g.add.sprite(65*i + invBtn.xOffset + 20, invBtn.yOffset + 15, this.g.objData[String(item)].sprite.file);
          icon.anchor.x = 0
          icon.anchor.y = 0
          icon.fixedToCamera = true;
          this.g.uiGroup.add(icon)
          invBtn.icon = icon
          invBtn.icon.tag = item
        }
      }
    }
  }

}

UI.createInvButtons = function (xOffset, yOffset) {

  var sendInvItemEquip = function(e) {
    var message = {};
    message.Action = "equipItem"
    message.Value = e.val
    console.log("Equipping item: ", e.val)
    this.g.sock.send(JSON.stringify(message))
  }

  for (i = 0; i < 7; i ++) {
    //add button
    button = this.g.add.button(65*i + xOffset, yOffset, 'inventory-empty-bg', sendInvItemEquip, this, 0, 0, 1);
    button.val = i;
    button.xOffset = xOffset;
    button.yOffset = yOffset;
    button.scale.setTo(0.17, 0.17);
    button.inputEnabled = true;
    button.fixedToCamera = true;
    this.g.uiGroup.add(button)
    this.invButtons.push(button)
    //add number text
    text = this.g.add.text(65*i + xOffset + 7, yOffset + 22, i+1, {fill: '#3498db'});
    text.anchor.setTo(0.5, 1);
    text.font = 'Revalia';
    text.fontSize = 15;
    text.fixedToCamera = true;
    this.g.uiGroup.add(text)
  }
}

UI.updateNearbyPlayerList = function() {
  for (i = 0; i < 5; i ++) {
    if (this.s.players[i]) {
      this.playerList[i].btn.visible = true
      this.playerList[i].txt.setText(this.s.players[i].username)
      this.playerList[i].btn.val = this.s.players[i].id
    } else {
      this.playerList[i].txt.setText("")
      this.playerList[i].btn.visible = false
    }
  }
}

UI.drawLine = function(p1, p2) {
  var dist = distance(p1, p2)
  var lineWidth = 100 * 2/dist
  this.g.graphics.lineStyle(lineWidth, 0x33FF00);
  this.g.graphics.moveTo(p1.x, p1.y);
  this.g.graphics.lineTo(p2.x, p2.y);
},

UI.drawConnectionLines = function() {
  this.g.graphics.clear()
  //check current player
  if (this.g.currentPlayer.parentId != null && this.g.currentPlayer.parentId != "") {
    parent = findObject(this.g.currentPlayer.parentId, this.s.players)
    if (parent != null) {
      this.drawLine(parent.entity, this.g.currentPlayer)
    }
  }
  //check other players
  for (i = 0; i < this.s.players.length; i++) {
    p = this.s.players[i]
    if (p.entity.parentId != null && p.entity.parentId != "") {

      parent = findObject(p.entity.parentId, this.s.players)
      if (parent != null) {
        this.drawLine(g, p.entity, parent.entity)
      } else if (p.entity.parentId == this.currentPlayer.id) {
        this.drawLine(g, p.entity, this.g.currentPlayer)
      }
    }
  }
}

UI.newInputField = function() {

}

UI.newButton = function() {

}
