
Diericx.MainMenu = function (game) {

	this.music = null;
	this.playButton = null;

};

Diericx.MainMenu.prototype = {

	create: function () {

		var loginFunc = this.login
		var thisVar = this

		//	We've already preloaded our assets, so let's kick right into the Main Menu itself.
		//	Here all we're doing is playing some music and adding a picture and button
		//	Naturally I expect you to do something significantly better :)

		// this.music = this.add.audio('titleMusic');
		// this.music.play();

		//this.add.sprite(0, 0, 'titlepage');

		//this.playButton = this.add.button(this.world.centerX - 95, 400, 'button', this.startGame, this, 2, 1, 0);
		//this.playButton = this.add.button(400, 600, 'playButton', this.startGame, this, 'buttonOver', 'buttonOut', 'buttonOver');

		var usernameField = document.getElementById("usernameField")
		//usernameField.style.left = this.world.centerX - (usernameField.offsetWidth/2) + "px"
		//usernameField.style.top = 100 + "px"

		var emailField = document.getElementById("emailField")
		//emailField.style.left = this.world.centerX - (emailField.offsetWidth/2) + "px"
		//emailField.style.top = 200 + "px"

		var passwordField = document.getElementById("passwordField")
		//passwordField.style.left = this.world.centerX - (passwordField.offsetWidth/2) + "px"
		//passwordField.style.top = 300 + "px"

		var submitBtn = document.getElementById("submitBtn")
		//submitBtn.style.left = this.world.centerX - (submitBtn.offsetWidth/2) + "px"
		//submitBtn.style.top = 400 + "px"

		$('#loginForm').submit(function () {
			loginFunc(usernameField.value, emailField.value, passwordField.value, thisVar);
			return false;
		});

	},

	update: function () {

		//	Do some nice funky main menu effect here

	},

	login: function(username, email, password, thisVar) {
    console.log(username + ", " + email +", " + password)
    // Create a callback to handle the result of the authentication
    function registerAuthHandler (error, authData) {
      console.log("Auth Callback...")
      if (error) {
        console.log("Login Failed!", error);
        errorMessage = error;
      } else {
        console.log("Authenticated successfully1 with payload:", authData);

        Diericx.token = authData.token

        var ref = new Firebase("https://diericx.firebaseio.com/users");
        ref.child(authData.uid).set({
          email: email,
          username: username
        });

				$('#ui').toggle();
				thisVar.state.start('Game');
      }
    }

    function loginAuthHandler(error, authData) {
      console.log("Auth Callback...")
      if (error) {
        console.log("Login Failed!", error);
        errorMessage = error;
      } else {
        console.log("Authenticated successfully2 with payload:", authData);
        Diericx.Token = authData.token
        Diericx.Uid = authData.uid

        $('#ui').toggle();
				thisVar.state.start('Game');
      }
    }

    // create firebase ref
    var ref = new Firebase("https://diericx.firebaseio.com");
    //authenticate
    if (username == "") {

      //login
      ref.authWithPassword({
        email    : email,
        password : password
      }, loginAuthHandler);

    } else {

      //register
      var ref = new Firebase("https://diericx.firebaseio.com");

      ref.createUser({
        email: email,
        password: password
        },
        function(error, userData) {
          //display error message
          if (error) {
            switch (error.code) {
              case "EMAIL_TAKEN":
                errorMessage = "That email is already in use.";
                break;
              case "INVALID_EMAIL":
                errorMessage = "That email is not valid email.";
                break;
              default:
                errorMessage = "Error creating user:" + error;
            }

          } else {
            //on succesful acount creation
            console.log("Successfully created user account with uid:", userData.uid);

            //login to get the token so we can write the user's data
            ref.authWithPassword({
              email    : email,
              password : password
            }, registerAuthHandler);

          }
        });
      }
    },

};
